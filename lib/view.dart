import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'drawer.dart';
import 'model.dart';

class View extends StatelessWidget {
  const View(this.view, this.model, {Key? key, this.scaffoldKey}) :
      super(key: key);

  final Widget view;
  final Model model;
  final Key? scaffoldKey;

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider(
      create: (context) => model,
      child: MaterialApp(
        title: 'Custom soundboard',
        home: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            // @todo update title - with Consumer of Model?
            title: const Text('Flutter Demo'),
          ),
          body: Consumer(
            builder: (context, Model model, child) {
              return view;
            }
          ),
          drawer: Consumer(
            builder: (context, Model model, child) {
              return FutureBuilder<Model>(
                future: model.fetch(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return DrawerView(snapshot.requireData);
                  } else if (snapshot.hasError) {
                    return Text('${snapshot.error}');
                  }
                  // By default, show a loading spinner.
                  return const CircularProgressIndicator();
                },
              );
            }
          ),
        ),
      ),
    );
  }
}


