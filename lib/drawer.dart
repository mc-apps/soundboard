import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:soundboard/settings.dart';

import '../model.dart';
import '../view.dart';
import 'main.dart';
import 'model.dart';

class DrawerView extends StatelessWidget {
  const DrawerView(this.model, {Key? key}) : super(key: key);

  final Model model;

  @override
  Widget build(BuildContext context) {
    const TextStyle headline = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
    const EdgeInsets padding1 = EdgeInsets.only(left:40);
    const EdgeInsets padding2 = EdgeInsets.only(left:60);

    List<Widget> list =[
      const DrawerHeader(
        decoration: BoxDecoration(
          color: Colors.blue,
        ),
        child: Text('Soundboard'),
      ),
      ListTile(
        title: const Text('Home'),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return View(Main(model), model);
            }),
          );
        },
      ),
      ListTile(
        title: const Text('Settings'),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return View(Settings(model), model);
            }),
          );
        },
      ),
    ];

    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: list,
      ),
    );
  }
}


