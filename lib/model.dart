import 'dart:developer' as developer;
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:soundboard/settings.dart';


class Model extends ChangeNotifier {

  Future<Model> fetch() async {
    return this;
  }

//
  // @todo initial with path
  Widget state = Settings.getInitialState();
  Directory? dir;

  void setLocalPath(String? path) async {
    if (path == null) {
      return;
    }

    state = Settings.getPendingState();
    notifyListeners();
    try {
      Directory dir = Directory(path);
      //StorageFilesystem storage = StorageFilesystem(path);
      dir.list();
      developer.log((await dir.list(recursive: true, followLinks: true).toSet()).toString());
      state = Settings.getSuccessState(path);
      this.dir = dir;
      notifyListeners();
      return;
    } catch (e){
      state = Settings.getFailedState();
      notifyListeners();
      developer.log('connection had error:');
      developer.log(e.toString());
      return;
    }
  }

}