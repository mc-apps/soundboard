import 'dart:convert';
import 'dart:developer' as developer;
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:soundboard/view.dart';
import 'package:soundboard/model.dart';
import 'package:path/path.dart';



void main() {
  Model model = Model();
  runApp(View(Main(model), model));
}
/*
class Main extends StatefulWidget {
  const Main(this.model, {Key? key}) : super(key: key);

  final Model model;

  @override
  _Main createState() => _Main(model);
}

class _Main extends State<Main> {
  _Main(this.model) {*/
class Main extends StatelessWidget {
  Main(this.model) {

    List<Object> filesAndFolders = model.dir?.listSync().toList()??[];

    filesAndFolders.forEach((element) {
      if (element is File) {
        if (basename(element.path) == 'config.json') {
          // @todo there is no permission to access anything else than medias
          developer.log('config file found');
          config.addAll(_get(element));
        } else {
          files.add(element);
        }
      }
    });
    developer.log('config loaded');
    // @todo
    sortByFile();
    developer.log('list done');
  }

  final Model model;
  final AudioPlayer audioPlayer = AudioPlayer();
  final List<ListTile> children = [];
  Map<dynamic, dynamic> config = foo;
  List<File> files = [];

  // folder
  Map<dynamic, dynamic> _get(File file) {
    try {
      developer.log(file.readAsStringSync());
      return const JsonDecoder().convert(
          file.readAsStringSync()
      );
    } catch (e) {
      developer.log('error getting "'+file.path+'":');
      developer.log(e.toString());
    }
    return {};
  }

  Future<void> playLocal(path) async {
    developer.log('play '+path);
    int result = await audioPlayer.play(path, isLocal: true);
    developer.log(result.toString());
  }

  void sortByFile() {
    files.sort((File a, File b){
      int length = a.path.length.compareTo(b.path.length);
      if (length == 0 ) {
        return a.path.compareTo(b.path);
      }
      return length;
    });


    developer.log('files loaded');

    files.forEach((element) {
      String name = basenameWithoutExtension(element.path);
      String text;
      if (config.containsKey(name)) {
        text = name + ' - ' + config[name].toString();
      } else {
        text = name;
      }
      // ReorderableListView
      children.add(ListTile(
        title: Text(text),
        //title: SingleChildScrollView(child: Text(basenameWithoutExtension(element.path))),
        onTap: () => playLocal(element.path),
        //child: null,
      ));
    });
  }

  void sortByText() {
    files.sort((File a, File b){
      String nameA = basenameWithoutExtension(a.path);
      String nameB = basenameWithoutExtension(b.path);
      if (config.containsKey(nameA)) {
        if (config.containsKey(nameB)) {
          return config[nameA]!.compareTo(config[nameB]!);
        } else {
          return -1;
        }
      } else if (config.containsKey(nameB)) {
        return 1;
      }
      int length = a.path.length.compareTo(b.path.length);
      if (length == 0 ) {
        return a.path.compareTo(b.path);
      }
      return length;
    });


    developer.log('files loaded');

    files.forEach((element) {
      String name = basenameWithoutExtension(element.path);
      String text;
      if (config.containsKey(name)) {
        text = name + ' - ' + config[name].toString();
      } else {
        text = name;
      }
      // ReorderableListView
      children.add(ListTile(
        title: Text(text),
        //title: SingleChildScrollView(child: Text(basenameWithoutExtension(element.path))),
        onTap: () => playLocal(element.path),
        //child: null,
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    developer.log('build');
    return Column(
      children: [
        Row(
          children: [
            ElevatedButton.icon(
              onPressed: () => sortByFile(),
              icon: const Icon(Icons.sort_by_alpha),
              label: Text('Filename'),
            ),
            ElevatedButton.icon(
              onPressed: () => sortByFile(),
              icon: const Icon(Icons.sort_by_alpha),
              label: Text('Text'),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
            itemCount:children.length,
            itemBuilder: (context, i) {
              return children[i];
          }),
        ),
        IconButton(
            onPressed: () => audioPlayer.stop(),
            icon: const Icon(Icons.pause_circle),
        ),
      ],
    );
    return ListView(
        children: children,
        padding: const EdgeInsets.all(8),
    );
  }
}

Map<dynamic, dynamic> foo = {
"1122":" der sieg ist euer",
"1114":" ein feindliches schiff ist in euere gewässer eingedrungen",
"1106":" fanfare - attakie",
"1098":" der feind wurde gesichtet",
"1066":" MArkthallen erweitern eueren einzugsbereich",
"1060":" zu Produktion von wolle bötigt ihr eine schaffarm",
"1016":" Ihr seid gescheitert",
"1014":" Ihr ward eueren Aufgaben nicht gewachsen",
"1012":" Ihr habt versagt",
"1006":" Ihr kontet eueren Auftrag nciht erfüllen",
"998":" ihr seid Bankrott",
"996":" Ihr habt eueren Aufrag erfüllt",
"994":" Auftrag erfolgreich ausgeführt",
"988":" Ihr ward erfolgreich",
"979":" Ihr legt eine Pause ein",
"975":" Netwerkverbindung hergestellt",
"973":" Warte auf Netwerkverbindung",
"971":" die Netwerkverbindung wurde unterbrochen",
"957":" Ein mitspieler hat das spiel verlassen",
"930":" Euch wurde der Kreig erklärt",
"908":" Ihr habt euch euinen Feind gemacht",
"903":" Der Auftrag wird ausgeführt",
"891":" Ihr soltet besser Schutzgeld bezahlen",
"883":" Versengt die Landratten",
"873":" Laaand in siiiicht",
"867":" Beim Klabautermann",
"861":" In die Wanten",
"859":" Hart am wind",
"853":" Aj Aj Captain",
"851":" Ajoooooiiii",
"845":" Für land und König",
"843":" Vorwärts",
"841":" Mir nach",
"839":" Gebt feuer",
"837":" Feueeeer",
"835":" Zu diensten mein Fürst",
"833":" ergebt euch",
"829":" zu Befehl",
"825":" Jawohl mein Fürst",
"813":" Für unseren Herrscher",
"738":" Einheit Ausgebildet ...",
"727":" ...",
"701":" Ihr habt den Kampf verloren",
"695":" Ihr habt eine Insel verloren",
"685":" Man zerstört euere Städte",
"681":" Ihr werdet angegriffen",
"666":" Euer schiff wurde versenkt",
"645":" Ihr besitzt genug schiffe",
"620":" Niemand will emhr euere waren kaufen",
"610":" Überprüft euere HAndelsrouten",
"606":" Route festgelegt",
"591":" Rohstoffe fehlen ...",
"581":" In einem Betrieb stapeln sich die Waren",
"544":" (Fremde stadt x000 einwohner)",
"530":" (Neue Siedlung)",
"526":" Euere Bürger können die Steuern nicht mehr bezahlen",
"524":" die steuern sind zu hoch",
"506":" Euere herrschaft ist schlecht",
"502":" Ihr erfüllt die bedüfnisse eueres Volkes nicht",
"500":" Ihr solltet die bedüfnisse eueres Volkes erfüllen",
"498":" Euere Bürger sind unzufrieden",
"486":" Kleidungstücke sind Mangelware",
"484":" fehlt an KAkao",
"482":" KAkao ist Mangelware",
"480":" Tabak geht zur neige",
"476":" gewürze",
"472":" Alkohol",
"468":" Stoffe",
"464":" Euer Volk Hungert",
"462":" Es fehlt an nahrung",
"460":" Nahrungsvorräte gehen zu neige",
"448":" Euer Volk braucht eiunen Arzt",
"440":" Schule",
"430":" Kirche",
"422":" Wirshaus",
"395":" Holz fehlt",
"387":" Ihr habt nicht genug Gold",
"383":" Das Volk bekommt kein Baumaterial",
"381":" Baumaterial entzogen",
"350":" Stadt hat x000 erreicht",
"323":" Ein Galgen Fehlt",
"309":" Ein Feuer ist asugebroichen",
"307":" Die Hütte qualmt, das Feuer schwelt, feurio ein Brunnen fehlt ...",
"291":" ein Arzt fehlt",
"278":" ein schloss wurde errichtet ...",
"270":" ...",
"260":" Kathedrale",
"250":" Triumpfbogen",
"239":" Denkmal",
"234":" Das volk hat genug von euerer herrschaft",
"232":" Der Pöbel will euch stürzen...",
"226":" ...",
"211":" Erz Miene leer",
"208":" unser fluch soll euch treffen ...",
"198":" ...",
"187":" Dürre",
"183":" Schatz ...",
"177":" ...",
"171":" Ureinwohner ...",
"167":" ...",
"153":" Gold ...",
"141":" Erz ...",
"133":" Ekundet",
"128":" Spiel verlassen",
"127":" Bildschirmauflösung wird geändert",
"123":" gesichert",
"107":" Wählt euer Banner",
"10":" Anno 1602",
"12":" Anno 1602",
"19":" Anno 1602",
"19":" Geschichte ...",
"54":" ..."
};
