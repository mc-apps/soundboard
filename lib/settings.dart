import 'dart:developer' as developer;

import 'package:flutter/material.dart';
//import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'package:file_picker/file_picker.dart';

import '../model.dart';
import 'model.dart';


//class Settings extends StatefulWidget {
class Settings extends StatelessWidget {
  const Settings(this.model, {Key? key}) : super(key: key);
  static Widget getInitialState() => const Text('...');
  static Widget getPendingState() => const CircularProgressIndicator();
  static Widget getSuccessState(String path) => /*const*/ Text('test successful, new path: '+path);
  static Widget getFailedState() => const Text('test was not successful');

  final Model model;
/*
  @override
  _SettingsViewState createState() => _SettingsViewState(model);
}

class _SettingsViewState extends State<Settings> {
  _SettingsViewState(this.model);

  final Model model;
*/

  void _selectFolder() {
    //if (await Permission.storage.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
        FilePicker.platform.getDirectoryPath().then((value) {
          developer.log(value??'');
          model.setLocalPath(value);
        });
    //}
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => model,
      child: Padding(
        padding: const EdgeInsets.all(12),
        // SingleChildScrollView
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[

            ElevatedButton(
              //style: full,
              onPressed: _selectFolder,
              child: const Text('SelectFolder&Save'),
            ),
            Consumer<Model>(
              builder: (context, Model model, child) {
                return model.state;
              }
            ),
          ],
        ),
      ),
    );
  }
}
